require 'test_helper'

class SchedulesControllerTest < ActionController::TestCase
  setup do
    @schedule = schedules(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:schedules)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create schedule" do
    assert_difference('Schedule.count') do
      post :create, schedule: { begin: @schedule.begin, end: @schedule.end, lu1112: @schedule.lu1112, lu12: @schedule.lu12, lu1314: @schedule.lu1314, lu34: @schedule.lu34, lu56: @schedule.lu56, lu78: @schedule.lu78, lu910: @schedule.lu910, permanent: @schedule.permanent }
    end

    assert_redirected_to schedule_path(assigns(:schedule))
  end

  test "should show schedule" do
    get :show, id: @schedule
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @schedule
    assert_response :success
  end

  test "should update schedule" do
    patch :update, id: @schedule, schedule: { begin: @schedule.begin, end: @schedule.end, lu1112: @schedule.lu1112, lu12: @schedule.lu12, lu1314: @schedule.lu1314, lu34: @schedule.lu34, lu56: @schedule.lu56, lu78: @schedule.lu78, lu910: @schedule.lu910, permanent: @schedule.permanent }
    assert_redirected_to schedule_path(assigns(:schedule))
  end

  test "should destroy schedule" do
    assert_difference('Schedule.count', -1) do
      delete :destroy, id: @schedule
    end

    assert_redirected_to schedules_path
  end
end
