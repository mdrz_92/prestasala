json.extract! schedule, :id, :permanent, :begin, :end, :lu12, :lu34, :lu56, :lu78, :lu910, :lu1112, :lu1314, :created_at, :updated_at
json.url schedule_url(schedule, format: :json)