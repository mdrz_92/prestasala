class Space < ActiveRecord::Base
	has_many :schedules
	has_many :requests
end
