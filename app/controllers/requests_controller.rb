class RequestsController < ApplicationController
  before_action :set_request, only: [:show, :edit, :update, :destroy]
  before_action :set_search

  # GET /requests
  # GET /requests.json
  def index
    @requests = Request.all

  end

  # GET /requests/1
  # GET /requests/1.json
  def show
  end

  # GET /requests/new
  def new
    @request = Request.new

    @request.block1=true
    @request.block2=true
    @request.block3=true
    @request.block4=true
    @request.block5=true
    @request.block6=true
    @request.block7=true
    @request.block8=true
    @request.block9=true
    @year = [params[:q]["begin_eq(1i)"]].reject(&:blank?) unless params[:q].nil?
    @month = [params[:q]["begin_eq(2i)"]].reject(&:blank?) unless params[:q].nil?
    @day = [params[:q]["begin_eq(3i)"]].reject(&:blank?) unless params[:q].nil?
    @space_name= [params[:q][:space_name_eq]].reject(&:blank?) unless params[:q].nil?

    @q = Schedule.search(params[:q])
    @results = @q.result(distinct: true)

    @results.each do |r|

        if r.block1==true 
          @request.block1=false   
        end  
        if r.block2==true 
          @request.block2=false     
        end 
        if r.block3==true 
          @request.block3=false     
        end 
        if r.block4==true 
          @request.block4=false     
        end 
        if r.block5==true 
          @request.block5=false      
        end 
        if r.block6==true 
          @request.block6=false     
        end 
        if r.block7==true 
          @request.block7=false      
        end 
        if r.block8==true 
          @request.block8=false      
        end 
        if r.block9==true 
          @request.block9=false      
        end
    end
   
  end

  # GET /requests/1/edit
  def edit
  end

  # POST /requests
  # POST /requests.json
  def create
    @request = Request.new(request_params)

    respond_to do |format|
      if @request.save
        format.html { redirect_to @request, notice: 'Request was successfully created.' }
        format.json { render :show, status: :created, location: @request }
      else
        format.html { render :new }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /requests/1
  # PATCH/PUT /requests/1.json
  def update
    respond_to do |format|
      if @request.update(request_params)
        format.html { redirect_to @request, notice: 'Request was successfully updated.' }
        format.json { render :show, status: :ok, location: @request }
      else
        format.html { render :edit }
        format.json { render json: @request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requests/1
  # DELETE /requests/1.json
  def destroy
    @request.destroy
    respond_to do |format|
      format.html { redirect_to requests_url, notice: 'Request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def set_search
    @search = Space.search(params[:params])
    @requests = @search.result
    @search.build_condition
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request
      @request = Request.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_params
      params.require(:request).permit(:daterequest, :confirmation, :finishrequest, :permanent, :block1, :block2, :block3, :block4, :block5, :block6, :block7, :block8, :block9, :space_id, :mandated, :mail, :description, :proyector, :pc, :audio, :parking, :numberparking)
    end

    def q_params
      params.require(:q).permit(:begin_eq, :space_name_eq)
    end

    def find_duplicates(q, request)
      find = Schedule.new

      18.times do |i|
        #@q.begin_eq = @q.begin_eq + ((i+1)*7).days
        

        @q = Schedule.where(:begin => find.begin + ((i+1)*7).days, :space_name => find.space_name )

      end
    end
end
