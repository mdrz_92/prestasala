class SchedulesController < ApplicationController
  before_action :set_schedule, only: [:show, :edit, :update, :destroy]

  # GET /schedules
  # GET /schedules.json
  def index
    @schedules = Schedule.all
    @spaces = Space.all
    @count_schedules = Schedule.count 
    @q = Schedule.ransack(params[:q])
    @events = @q.result(distinct: true)

  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
  end

  # GET /schedules/new
  def new
    @schedule = Schedule.new
  end

  # GET /schedules/1/edit
  def edit
  end

  # POST /schedules
  # POST /schedules.json
  def create
    @schedule = Schedule.new(schedule_params)

    respond_to do |format|
      if @schedule.save
        if @schedule.permanent==true

          duplicated_schedule(@schedule)
          
        end

        format.html { redirect_to @schedule, notice: 'Schedule was successfully created.' }
        format.json { render :show, status: :created, location: @schedule }
      else
        format.html { render :new }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schedules/1
  # PATCH/PUT /schedules/1.json
  def update
    respond_to do |format|
      if @schedule.update(schedule_params)
        format.html { redirect_to @schedule, notice: 'Schedule was successfully updated.' }
        format.json { render :show, status: :ok, location: @schedule }
      else
        format.html { render :edit }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to schedules_url, notice: 'Schedule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def remove_all
      Schedule.delete_all
      flash[:notice] = "Has borrado todos los resultados"
      redirect_to schedules_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      @schedule = Schedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_params
      params.require(:schedule).permit(:permanent, :begin, :block1, :block2, :block3, :block4, :block5, :block6, :block7, :block8, :block9, :space_id, :mandated, :mail, :description, :proyector, :pc, :audio, :parking, :numberparking)
    end

    def duplicated_schedule(schedule)
      18.times do |i|
        @new_schedule =@schedule.dup
        #@new_schedule.permanent = false
        @new_schedule.begin = @new_schedule.begin + ((i+1)*7).days
        @new_schedule.save
      end
    end

end
