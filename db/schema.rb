# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160824215559) do

  create_table "requests", force: :cascade do |t|
    t.datetime "daterequest"
    t.boolean  "confirmation"
    t.datetime "finishrequest"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "permanent"
    t.boolean  "block1"
    t.boolean  "block2"
    t.boolean  "block3"
    t.boolean  "block4"
    t.boolean  "block5"
    t.boolean  "block6"
    t.boolean  "block7"
    t.boolean  "block8"
    t.boolean  "block9"
    t.string   "mandated",      default: "",    null: false
    t.string   "mail",          default: "",    null: false
    t.text     "description"
    t.boolean  "proyector",     default: false, null: false
    t.boolean  "pc",            default: false, null: false
    t.boolean  "audio",         default: false, null: false
    t.boolean  "parking",       default: false, null: false
    t.integer  "numberparking", default: 0,     null: false
    t.integer  "space_id"
  end

  add_index "requests", ["space_id"], name: "index_requests_on_space_id"

  create_table "schedules", force: :cascade do |t|
    t.boolean  "permanent"
    t.datetime "begin"
    t.boolean  "block1"
    t.boolean  "block2"
    t.boolean  "block3"
    t.boolean  "block4"
    t.boolean  "block5"
    t.boolean  "block6"
    t.boolean  "block7"
    t.boolean  "block8"
    t.boolean  "block9"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "space_id"
    t.string   "mandated",      default: "",    null: false
    t.string   "mail",          default: "",    null: false
    t.text     "description"
    t.boolean  "proyector",     default: false, null: false
    t.boolean  "pc",            default: false, null: false
    t.boolean  "audio",         default: false, null: false
    t.boolean  "parking",       default: false, null: false
    t.integer  "numberparking", default: 0,     null: false
  end

  add_index "schedules", ["space_id"], name: "index_schedules_on_space_id"

  create_table "spaces", force: :cascade do |t|
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "name",          default: "",    null: false
    t.integer  "capacity",      default: 0,     null: false
    t.boolean  "proyector",     default: false, null: false
    t.boolean  "pc",            default: false, null: false
    t.boolean  "audio",         default: false, null: false
    t.text     "reason",        default: "",    null: false
    t.text     "description"
    t.integer  "kind",          default: 0,     null: false
    t.boolean  "parking"
    t.integer  "numberparking"
    t.text     "other"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "name",                   default: "",   null: false
    t.string   "carrer",                 default: "",   null: false
    t.string   "rut",                    default: "",   null: false
    t.boolean  "student",                default: true
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
