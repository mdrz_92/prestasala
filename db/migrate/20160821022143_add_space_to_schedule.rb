class AddSpaceToSchedule < ActiveRecord::Migration
  def change
    add_reference :schedules, :space, index: true, foreign_key: true
  end
end
