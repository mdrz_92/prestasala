class RenameColumnSpace < ActiveRecord::Migration
  def change
  	rename_column :spaces, :type, :kind
  end
end
