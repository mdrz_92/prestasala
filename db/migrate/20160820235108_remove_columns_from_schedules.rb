class RemoveColumnsFromSchedules < ActiveRecord::Migration
  def change
  	remove_column :schedules, :ma12, :boolean
  	remove_column :schedules, :ma34, :boolean
  	remove_column :schedules, :ma56, :boolean
  	remove_column :schedules, :ma78, :boolean
  	remove_column :schedules, :ma910, :boolean
  	remove_column :schedules, :ma1112, :boolean
  	remove_column :schedules, :ma1314, :boolean
  	remove_column :schedules, :ma1516, :boolean
  	remove_column :schedules, :ma1718, :boolean

  	remove_column :schedules, :mi12, :boolean
  	remove_column :schedules, :mi34, :boolean
  	remove_column :schedules, :mi56, :boolean
  	remove_column :schedules, :mi78, :boolean
  	remove_column :schedules, :mi910, :boolean
  	remove_column :schedules, :mi1112, :boolean
  	remove_column :schedules, :mi1314, :boolean
  	remove_column :schedules, :mi1516, :boolean
  	remove_column :schedules, :mi1718, :boolean
  end
end
