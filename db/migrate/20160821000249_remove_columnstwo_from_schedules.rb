class RemoveColumnstwoFromSchedules < ActiveRecord::Migration
  def change

  	remove_column :schedules, :ju12, :boolean
  	remove_column :schedules, :ju34, :boolean
  	remove_column :schedules, :ju56, :boolean
  	remove_column :schedules, :ju78, :boolean
  	remove_column :schedules, :ju910, :boolean
  	remove_column :schedules, :ju1112, :boolean
  	remove_column :schedules, :ju1314, :boolean
  	remove_column :schedules, :ju1516, :boolean
  	remove_column :schedules, :ju1718, :boolean

  	remove_column :schedules, :vi12, :boolean
  	remove_column :schedules, :vi34, :boolean
  	remove_column :schedules, :vi56, :boolean
  	remove_column :schedules, :vi78, :boolean
  	remove_column :schedules, :vi910, :boolean
  	remove_column :schedules, :vi1112, :boolean
  	remove_column :schedules, :vi1314, :boolean
  	remove_column :schedules, :vi1516, :boolean
  	remove_column :schedules, :vi1718, :boolean

  	remove_column :schedules, :sa12, :boolean
  	remove_column :schedules, :sa34, :boolean
  	remove_column :schedules, :sa56, :boolean
  	remove_column :schedules, :sa78, :boolean
  	remove_column :schedules, :sa910, :boolean
  	remove_column :schedules, :sa1112, :boolean
  	remove_column :schedules, :sa1314, :boolean
  	remove_column :schedules, :sa1516, :boolean
  	remove_column :schedules, :sa1718, :boolean

  	remove_column :schedules, :end, :datetime


  end
end
