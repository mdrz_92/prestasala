class AddElementsToRequests < ActiveRecord::Migration
  def change
  	add_column :requests, :permanent, :boolean
  	add_column :requests, :block1, :boolean
  	add_column :requests, :block2, :boolean
  	add_column :requests, :block3, :boolean
  	add_column :requests, :block4, :boolean
  	add_column :requests, :block5, :boolean
  	add_column :requests, :block6, :boolean
  	add_column :requests, :block7, :boolean
  	add_column :requests, :block8, :boolean
  	add_column :requests, :block9, :boolean
  	add_column :requests, :mandated, :string, null: false, default: ""
  	add_column :requests, :mail, :string, null: false, default: ""
  	add_column :requests, :description, :text
  	add_column :requests, :proyector, :boolean, null: false, default: false
  	add_column :requests, :pc, :boolean, null: false, default: false
  	add_column :requests, :audio, :boolean, null: false, default: false
  	add_column :requests, :parking, :boolean, null: false, default: false
  	add_column :requests, :numberparking, :integer, null: false, default: 0
  	add_reference :requests, :space, index: true, foreign_key: true
  end
end
