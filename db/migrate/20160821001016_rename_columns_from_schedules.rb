class RenameColumnsFromSchedules < ActiveRecord::Migration
  def change
  	rename_column :schedules, :lu12, :block1
  	rename_column :schedules, :lu34, :block2
  	rename_column :schedules, :lu56, :block3
  	rename_column :schedules, :lu78, :block4
  	rename_column :schedules, :lu910, :block5
  	rename_column :schedules, :lu1112, :block6
  	rename_column :schedules, :lu1314, :block7
  	rename_column :schedules, :lu1516, :block8
  	rename_column :schedules, :lu1718, :block9
  end
end
