class AddColumnsToSchedules < ActiveRecord::Migration
  def change
  	add_column :schedules, :mandated, :string, null: false, default: ""
  	add_column :schedules, :mail, :string, null: false, default: ""
  	add_column :schedules, :description, :text
  	add_column :schedules, :proyector, :boolean, null: false, default: false
  	add_column :schedules, :pc, :boolean, null: false, default: false
  	add_column :schedules, :audio, :boolean, null: false, default: false
  	add_column :schedules, :parking, :boolean, null: false, default: false
  	add_column :schedules, :numberparking, :integer, null: false, default: 0
  end
end
