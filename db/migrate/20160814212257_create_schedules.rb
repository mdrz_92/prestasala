class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.boolean :permanent
      t.datetime :begin
      t.datetime :end
      t.boolean :lu12
      t.boolean :lu34
      t.boolean :lu56
      t.boolean :lu78
      t.boolean :lu910
      t.boolean :lu1112
      t.boolean :lu1314
      t.boolean :lu1516
      t.boolean :lu1718
      t.boolean :ma12
      t.boolean :ma34
      t.boolean :ma56
      t.boolean :ma78
      t.boolean :ma910
      t.boolean :ma1112
      t.boolean :ma1314
      t.boolean :ma1516
      t.boolean :ma1718
      t.boolean :mi12
      t.boolean :mi34
      t.boolean :mi56
      t.boolean :mi78
      t.boolean :mi910
      t.boolean :mi1112
      t.boolean :mi1314
      t.boolean :mi1516
      t.boolean :mi1718
      t.boolean :ju12
      t.boolean :ju34
      t.boolean :ju56
      t.boolean :ju78
      t.boolean :ju910
      t.boolean :ju1112
      t.boolean :ju1314
      t.boolean :ju1516
      t.boolean :vi1718
      t.boolean :vi12
      t.boolean :vi34
      t.boolean :vi56
      t.boolean :vi78
      t.boolean :vi910
      t.boolean :vi1112
      t.boolean :vi1314
      t.boolean :vi1516
      t.boolean :vi1718
      t.boolean :sa12
      t.boolean :sa34
      t.boolean :sa56
      t.boolean :sa78
      t.boolean :sa910
      t.boolean :sa1112
      t.boolean :sa1314
      t.boolean :sa1516
      t.boolean :sa1718

      t.timestamps null: false
    end
  end
end
