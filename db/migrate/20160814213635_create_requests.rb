class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.datetime :daterequest
      t.boolean :confirmation
      t.datetime :finishrequest

      t.timestamps null: false
    end
  end
end
