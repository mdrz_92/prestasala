class AddRegistrationInfoToUser < ActiveRecord::Migration
  def change
    add_column :users, :name, :string, null: false, default: ""
    add_column :users, :carrer, :string, null: false, default: ""
    add_column :users, :rut, :string, null: false, default: ""
    add_column :users, :student, :boolean, default: true

  end
end
