class AddAtributesToSpace < ActiveRecord::Migration
  def change
    add_column :spaces, :name, :string, null: false, default: ""
    add_column :spaces, :capacity, :integer, null: false, default: 0
    add_column :spaces, :proyector, :boolean, null: false, default: false
    add_column :spaces, :pc, :boolean, null: false, default: false
    add_column :spaces, :audio, :boolean, null: false, default: false
    add_column :spaces, :reason, :text, null: false, default: ""
    add_column :spaces, :description, :text
    add_column :spaces, :type, :integer, null: false, default: 0
    add_column :spaces, :parking, :boolean
    add_column :spaces, :numberparking, :integer
    add_column :spaces, :other, :text
  end
end
